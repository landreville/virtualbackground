from PIL import Image
import tfjs_graph_converter as tfjs
import tensorflow as tf
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"


def bodypix(graph, img, bg_img):

    x = tf.keras.preprocessing.image.img_to_array(img, dtype=np.float32)

    # add imagenet mean - extracted from body-pix source
    m = np.array([-123.15, -115.90, -103.06])
    x = np.add(x, m)
    sample_image = x[tf.newaxis, ...]

    with tf.Session(graph=graph) as sess:
        input_tensor_names = tfjs.util.get_input_tensors(graph)
        output_tensor_names = tfjs.util.get_output_tensors(graph)
        input_tensor = graph.get_tensor_by_name(input_tensor_names[0])
        results = sess.run(output_tensor_names, feed_dict={input_tensor: sample_image})

    segments = np.squeeze(results[6], 0)
    segmentation_threshold = 0.7
    segment_scores = tf.sigmoid(segments)
    mask = tf.math.greater(segment_scores, tf.constant(segmentation_threshold))

    # mask = cv2.dilate(mask, np.ones((10, 10), np.uint8), iterations=1)
    # mask = cv2.erode(mask, np.ones((10, 10), np.uint8), iterations=1)

    segmentation_mask = tf.dtypes.cast(mask, tf.float32)
    segmentation_mask = np.reshape(
        segmentation_mask, (segmentation_mask.shape[0], segmentation_mask.shape[1])
    )

    mask_img = Image.fromarray(segmentation_mask * 255)
    mask_img = mask_img.resize(
        (1280, 720), Image.LANCZOS
    ).convert("RGB")
    mask_img = tf.keras.preprocessing.image.img_to_array(mask_img, dtype=np.uint8)
    segmentation_mask_inv = np.bitwise_not(mask_img)

    bg = np.bitwise_and(np.array(bg_img), np.array(segmentation_mask_inv))
    fg = np.bitwise_and(np.array(img), np.array(mask_img))

    new_img = np.bitwise_xor(bg, fg)

    return new_img


#
# plt.clf()
# plt.title('Segmentation Mask')
# plt.ylabel('y')
# plt.xlabel('x')
# plt.imshow(segmentationMask * OutputStride)
# plt.show()
#
# # Draw Segmented Output
# mask_img = Image.fromarray(segmentationMask * 255)
# mask_img = mask_img.resize(
#     (targetWidth, targetHeight), Image.LANCZOS).convert("RGB")
# mask_img = tf.keras.preprocessing.image.img_to_array(
#     mask_img, dtype=np.uint8)
#
# segmentationMask_inv = np.bitwise_not(mask_img)
# fg = np.bitwise_and(np.array(img), np.array(
#     mask_img))
# plt.title('Foreground Segmentation')
# plt.imshow(fg)
# plt.show()
# bg = np.bitwise_and(np.array(img), np.array(
#     segmentationMask_inv))
# plt.title('Background Segmentation')
# plt.imshow(bg)
# plt.show()
#
# output = {
#     'mask': segmentationMask * OutputStride,
#     'foreground': fg,
#     'segmentation_mask': segmentationMask,
#     'segmentation_mask_inv': segmentationMask_inv,
#
# }
#
# # Part Heatmaps, PartOffsets,
# for i in range(partHeatmaps.shape[2]):
#     if i >= len(PART_CHANNELS):
#         break
#
#     heatmap = partHeatmaps[:, :, i]  # First Heat map
#     heatmap[np.logical_not(tf.math.reduce_any(mask, axis=-1).numpy())] = -1
#     # Set portions of heatmap where person is not present in segmentation mask, set value to -1
#
#     # SHOW HEATMAPS
#
#     plt.clf()
#     plt.title('Heatmap: ' + PART_CHANNELS[i])
#     plt.ylabel('y')
#     plt.xlabel('x')
#     plt.imshow(heatmap * OutputStride)
#     plt.show()
#
#     output[PART_CHANNELS[i]] = heatmap * OutputStride
#
# return output
