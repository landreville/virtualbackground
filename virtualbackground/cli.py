import logging
import sys
import argparse
from .main import virtualize_background

log = logging.getLogger(__name__)


def main():
    root = logging.getLogger()
    root.setLevel(logging.INFO)

    pkg_log = logging.getLogger('virtualbackground')
    pkg_log.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    root.addHandler(handler)
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('model_path', metavar='MODEL_PATH')
    parser.add_argument('video_path', metavar='VIDEO_PATH')
    args = parser.parse_args()

    virtualize_background(**vars(args))
