from PIL import Image
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import logging
import cv2
import tensorflow as tf
import tfjs_graph_converter as tfjs
import numpy as np
import pyfakewebcam

from virtualbackground.evalbody_singleposemodel import bodypix

log = logging.getLogger(__name__)

height = 720
width = 1280


def virtualize_background(model_path, video_path):
    log.debug('Running video capture.')

    graph = tfjs.api.load_graph_model(model_path)

    bg_cap = cv2.VideoCapture(video_path)
    cap = cv2.VideoCapture(0)

    bg_cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    bg_cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    bg_cap.set(cv2.CAP_PROP_FPS, 60)
    bg_cap.set(cv2.CAP_PROP_POS_FRAMES, 180)

    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FPS, 60)

    try:
        capture_frames(graph, cap, bg_cap)
    except (KeyboardInterrupt, Exception):
        cap.release()
        cv2.destroyAllWindows()
        raise


def capture_frames(graph, cap, bg_cap):
    fake = pyfakewebcam.FakeWebcam('/dev/video20', width, height)
    segmentation_threshold = 0.5
    # add imagenet mean - extracted from body-pix source
    m = np.array([-123.15, -115.90, -103.06])

    with tf.compat.v1.Session(graph=graph) as sess:
        input_tensor_names = tfjs.util.get_input_tensors(graph)
        output_tensor_names = tfjs.util.get_output_tensors(graph)
        input_tensor = graph.get_tensor_by_name(input_tensor_names[0])

        while True:
            _, img = cap.read()
            _, bg_img = bg_cap.read()

            new_img = run_bodypix(bg_img, img, input_tensor, m, output_tensor_names,
                                  segmentation_threshold, sess)

            new_img = cv2.cvtColor(new_img, cv2.COLOR_BGR2RGB)
            fake.schedule_frame(new_img)


def run_bodypix(bg_img, img, input_tensor, m, output_tensor_names, segmentation_threshold,
                sess):
    x = tf.keras.preprocessing.image.img_to_array(img, dtype=np.float32)
    x = np.add(x, m)
    sample_image = x[tf.newaxis, ...]
    results = sess.run(
        output_tensor_names,
        feed_dict={input_tensor: sample_image}
    )
    segments = np.squeeze(results[6], 0)
    segment_scores = tf.sigmoid(segments)
    mask = tf.math.greater(segment_scores, tf.constant(segmentation_threshold))

    segmentation_mask = tf.dtypes.cast(mask, tf.float32)
    segmentation_mask = tf.reshape(
        segmentation_mask, (segmentation_mask.shape[0], segmentation_mask.shape[1])
    )
    segmentation_mask = segmentation_mask.eval()

    segmentation_mask = cv2.resize(
        segmentation_mask,
        dsize=(1280, 720),
        interpolation=cv2.INTER_CUBIC
    )

    # segmentation_mask = cv2.dilate(segmentation_mask, np.ones((10, 10), np.uint8), iterations=1)
    # segmentation_mask = cv2.erode(segmentation_mask, np.ones((10, 10), np.uint8), iterations=1)

    mask_img = Image.fromarray(segmentation_mask * 255)
    # mask_img = mask_img.resize(
    #     (1280, 720), Image.LANCZOS
    # )
    mask_img = mask_img.convert("RGB")

    mask_img = tf.keras.preprocessing.image.img_to_array(mask_img, dtype=np.uint8)
    segmentation_mask_inv = np.bitwise_not(mask_img)
    bg = np.bitwise_and(np.array(bg_img), np.array(segmentation_mask_inv))
    fg = np.bitwise_and(np.array(img), np.array(mask_img))
    new_img = np.bitwise_xor(bg, fg)
    return new_img
