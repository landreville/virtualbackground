import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()

requires = [
    'matplotlib',
    'opencv-python',
    'tensorflow',
]

tests_require = [
    'pytest >= 3.7.4',
    'pytest-cov',
]

setup(
    name='virtualbackground',
    version='0.0',
    description='Virtual background for video conferencing',
    long_description=README,
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.8',
    ],
    author='',
    author_email='',
    url='',
    keywords='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'virtualbackground=virtualbackground.cli:main'
        ]
    },
)
